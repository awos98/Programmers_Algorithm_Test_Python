def solution(numLog):
    ch_arr = [''] * 12
    ch_arr[-10], ch_arr[-1], ch_arr[1], ch_arr[10] = 'a', 's', 'w', 'd'
    answer = ''
    for i in range(len(numLog) - 1):
        answer += ch_arr[numLog[i + 1] - numLog[i]]
    return answer


print(solution([0, 1, 0, 10, 0, 1, 0, 10, 0, -1, -2, -1]))
